package alik.lean;

import static alik.lean.Exceptions.rethrowUnchecked;

public class Functional {

	public static <T, R> java.util.function.Function<T, R> unchecked(Function<T, R> f) {
		return Function.unchecked(f);
	}

	public static <T> java.util.function.Predicate<T> unchecked(Predicate<T> p) {
		return Predicate.unchecked(p);
	}

	public static <T> java.util.function.Consumer<T> unchecked(Consumer<T> c) {
		return Consumer.unchecked(c);
	}

	public static <T> java.util.function.Supplier<T> unchecked(Supplier<T> c) {
		return Supplier.unchecked(c);
	}

	public static <T> java.lang.Runnable unchecked(Runnable r) {
		return Runnable.unchecked(r);
	}

	public static <T, R> java.util.function.Function<T, R> toFunction(java.util.function.Supplier<R> supplier) {
		return r -> supplier.get();
	}

	public static <T> java.util.function.Function<T, Boolean> toFunction(java.util.function.Predicate<T> predicate) {
		return predicate::test;
	}

	public static <T, R> java.util.function.Function<T, R> toFunction(java.util.function.Consumer<T> consumer) {
		return t -> {
			consumer.accept(t);
			return null;
		};
	}

	public static <T, R> java.util.function.Function<T, R> toFunction(java.lang.Runnable runnable) {
		return r -> {
			runnable.run();
			return null;
		};
	}

	public static java.lang.Runnable toRunnable(java.util.function.Function<?, ?> function) {
		return () -> function.apply(null);
	}

	@FunctionalInterface
	public interface Function<T, R> {
		static <T, R> java.util.function.Function<T, R> unchecked(Function<T, R> f) {
			return t -> {
				try {
					return f.apply(t);
				} catch (Exception e) {
					throw rethrowUnchecked(e);
				}
			};
		}

		R apply(T t) throws Exception;
	}


	@FunctionalInterface
	public interface Predicate<T> {
		static <T> java.util.function.Predicate<T> unchecked(Predicate<T> p) {
			return t -> {
				try {
					return p.test(t);
				} catch (Exception e) {
					throw rethrowUnchecked(e);
				}
			};
		}

		boolean test(T t) throws Exception;
	}

	@FunctionalInterface
	public interface Consumer<T> {
		static <T> java.util.function.Consumer<T> unchecked(Consumer<T> c) {
			return t -> {
				try {
					c.accept(t);
				} catch (Exception e) {
					throw rethrowUnchecked(e);
				}
			};
		}

		void accept(T t) throws Exception;
	}

	@FunctionalInterface
	public interface Supplier<T> {
		static <T> java.util.function.Supplier<T> unchecked(Supplier<T> s) {
			return () -> {
				try {
					return s.get();
				} catch (Exception e) {
					throw rethrowUnchecked(e);
				}
			};
		}

		T get() throws Exception;
	}

	@FunctionalInterface
	public interface Runnable {
		static java.lang.Runnable unchecked(Runnable r) {
			return () -> {
				try {
					r.run();
				} catch (Exception e) {
					throw rethrowUnchecked(e);
				}
			};
		}

		void run() throws Exception;
	}
}
