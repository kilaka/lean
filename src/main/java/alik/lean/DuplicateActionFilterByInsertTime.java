package alik.lean;

import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * TODO: support number of actions in addition to time. Perhaps other purging policies?
 */
public class DuplicateActionFilterByInsertTime<E extends Runnable> {

	private static final Logger LOGGER = Logger.getLogger(DuplicateActionFilterByInsertTime.class.getName());

	private final long filterMillis;

	private final ConcurrentHashMap<E, FilterInfoImpl> actionMap = new ConcurrentHashMap<>();

	private final ConcurrentLinkedQueue<E> actionQueue = new ConcurrentLinkedQueue<>();

	private final ScheduledExecutorService scheduledExecutorService = new ScheduledThreadPoolExecutor(0);

	private final AtomicBoolean purgerRegistered = new AtomicBoolean(false);

	private final Set<Listener<E>> listeners = ConcurrentHashMap.newKeySet();

	public DuplicateActionFilterByInsertTime(int filterMillis) {
		this.filterMillis = filterMillis;
	}

	public FilterInfo get(E e) {
		FilterInfoImpl insertionData = actionMap.get(e);
		if (insertionData == null || insertionData.isExpired(filterMillis)) {
			return null;
		}
		return insertionData;
	}

	public boolean run(E e) {
		actionMap.computeIfPresent(e, (e1, insertionData) -> {
			int count = insertionData.incrementAndGet();
			if (count == 2) {
				notifyFilteringStarted(e1);
			}
			return insertionData;
		});
		boolean isNew = actionMap.computeIfAbsent(e, e1 -> {
			FilterInfoImpl insertionData = new FilterInfoImpl();
			actionQueue.add(e1);
			return insertionData;
		}).getCount() == 1;

		tryRegisterPurger();

		if (isNew) {
			e.run();
		}
		return isNew;
	}

	private void tryRegisterPurger() {
		if (actionMap.size() != 0 && purgerRegistered.compareAndSet(false, true)) {
			scheduledExecutorService.schedule(() -> {
				try {
					for (Iterator<E> iterator = actionQueue.iterator(); iterator.hasNext(); ) {
						E e = iterator.next();
						FilterInfoImpl insertionData = actionMap.get(e);
						if (insertionData == null || insertionData.isExpired(filterMillis)) {
							iterator.remove();
						}
						if (insertionData != null && insertionData.isExpired(filterMillis)) {
							FilterInfoImpl removed = actionMap.remove(e);
							FilteredItem<E> filteredItem = new FilteredItem<>(e, removed);
							notifyFilteringFinished(filteredItem);
						} else {
							// All the elements that were left shouldn't be purged.
							break;
						}
					}
				} finally {
					purgerRegistered.set(false);
					tryRegisterPurger();
				}
			}, filterMillis, TimeUnit.MILLISECONDS);
		}
	}

	private void notifyFilteringFinished(FilteredItem<E> filteredItem) {
		new Thread(() -> listeners.forEach(l -> {
			try {
				l.onFilteringFinished(filteredItem);
			} catch (Exception e) {
				LOGGER.log(Level.WARNING, "Purge notification failed. Continuing to next one (if exists)", e);
			}
		})).start();
	}

	private void notifyFilteringStarted(final E e) {
		new Thread(() -> listeners.forEach(l -> {
			try {
				l.onFilteringStarted(e);
			} catch (Exception e1) {
				LOGGER.log(Level.WARNING, "Filtering started notification failed. Continuing to next one (if exists)", e1);
			}
		})).start();
	}

	public void addListener(Listener<E> listener) {
		listeners.add(listener);
	}

	public void removeLister(Listener<E> listener) {
		listeners.remove(listener);
	}

	public interface FilterInfo {
		long getInsertTimeMillis();

		int getCount();
	}

	public interface Listener<E> {
		void onFilteringStarted(E e);
		void onFilteringFinished(FilteredItem<E> filteredItem);
	}

	private static class FilterInfoImpl implements FilterInfo {
		private final long insertTimeMillis = System.currentTimeMillis();
		private AtomicInteger count = new AtomicInteger(1);

		int incrementAndGet() {
			return count.incrementAndGet();
		}

		@Override
		public long getInsertTimeMillis() {
			return insertTimeMillis;
		}

		@Override
		public int getCount() {
			return count.get();
		}

		boolean isExpired(long expirationMillis) {
			return insertTimeMillis + expirationMillis < System.currentTimeMillis();
		}
	}

	public static class FilteredItem<E> {
		private final E item;
		private final FilterInfo filterInfo;

		FilteredItem(E item, FilterInfo filterInfo) {
			this.item = item;
			this.filterInfo = filterInfo;
		}

		public E getItem() {
			return item;
		}

		public FilterInfo getFilterInfo() {
			return filterInfo;
		}
	}
}
