package alik.lean.test;

class MyException extends Exception {

	static final String EXCEPTION_TEXT = "Exception text";

	MyException() {
		super(EXCEPTION_TEXT);
	}
}
