package alik.lean.test;

import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.IntStream;

import alik.lean.DuplicateActionFilterByInsertTime;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;

public class DuplicateActionFilterByInsertTimeTest {

	private final AtomicInteger numOfLoggedMessages = new AtomicInteger(0);

	@Test
	public void testSimple() throws InterruptedException {
		int filterMillis = 100;
		DuplicateActionFilterByInsertTime<Runnable> expSet = new DuplicateActionFilterByInsertTime<>(filterMillis);
		AtomicInteger purgeCount = new AtomicInteger(0);
		expSet.addListener(new DuplicateActionFilterByInsertTime.Listener<Runnable>() {
			@Override
			public void onFilteringFinished(DuplicateActionFilterByInsertTime.FilteredItem<Runnable> filteredItem) {
				purgeCount.incrementAndGet();
			}

			@Override
			public void onFilteringStarted(Runnable runnable) {
			}
		});

		Runnable key = () -> {
		};
		long beforeAddMillis = System.currentTimeMillis();
		boolean added = expSet.run(key);
		long afterAddMillis = System.currentTimeMillis();
		Assert.assertTrue(added);
		DuplicateActionFilterByInsertTime.FilterInfo filterInfo = expSet.get(key);
		Assertions.assertThat(filterInfo.getInsertTimeMillis()).isBetween(beforeAddMillis, afterAddMillis);

		expSet.run(key);
		DuplicateActionFilterByInsertTime.FilterInfo filterInfo2 = expSet.get(key);
		Assert.assertEquals(filterInfo.getInsertTimeMillis(), filterInfo2.getInsertTimeMillis());

		Assert.assertFalse(filterInfo.getInsertTimeMillis() + filterMillis < System.currentTimeMillis());
		Assert.assertEquals(filterInfo.getCount(), 2);

		Thread.sleep(filterMillis);

		Assertions.assertThat(expSet.get(key)).isNull();

		Assert.assertNull(expSet.get(key));

		Thread.sleep(filterMillis * 2); // Give a chance to purge the items.
		Assert.assertEquals(1, purgeCount.get());

		System.out.println("Finished");
	}

	@Test
	public void testPurge() throws InterruptedException {
		int filterMillis = 100;
		DuplicateActionFilterByInsertTime<Runnable> expSet = new DuplicateActionFilterByInsertTime<>(filterMillis);

		int numItems = 100;
		int numPurgeListeners = 10;

		AtomicInteger purgeCount = new AtomicInteger(0);

		IntStream.range(0, numItems).forEach(i -> expSet.run(new Runnable() {
			@SuppressWarnings("unused")
			private int a = i; // On purpose in order to create a new instance and not same lambda.

			@Override
			public void run() {
			}
		}));

		AtomicReference<DuplicateActionFilterByInsertTime.Listener<Runnable>> lastListener = new AtomicReference<>();

		IntStream.range(0, numPurgeListeners).forEach(i -> {
			DuplicateActionFilterByInsertTime.Listener<Runnable> listener = new DuplicateActionFilterByInsertTime.Listener<Runnable>() {
				@Override
				public void onFilteringFinished(DuplicateActionFilterByInsertTime.FilteredItem<Runnable> purgedItem) {
					purgeCount.incrementAndGet();
				}

				@Override
				public void onFilteringStarted(Runnable runnable) {
				}
			};
			expSet.addListener(listener);
			lastListener.set(listener);
		});

		expSet.removeLister(lastListener.get());

		Thread.sleep(filterMillis * 3); // Give a chance to purge the items.

		Assert.assertEquals(numItems * (numPurgeListeners - 1), purgeCount.get());
	}

	@Test
	public void testLogBombingUsage() throws InterruptedException {
		int filterMillis = 100;
		DuplicateActionFilterByInsertTime<LoggerMessage> expSet = new DuplicateActionFilterByInsertTime<>(filterMillis);

		Logger logger = Logger.getLogger(DuplicateActionFilterByInsertTimeTest.class.getName());
		LoggerMessage loggerMessage = new LoggerMessageThatIncrements(logger, Level.INFO, "Hello {0}", "Alik");


		//noinspection Duplicates
		expSet.addListener(new DuplicateActionFilterByInsertTime.Listener<LoggerMessage>() {
			@Override
			public void onFilteringFinished(DuplicateActionFilterByInsertTime.FilteredItem<LoggerMessage> filteredItem) {
				loggerMessage.getLogger().log(Level.INFO, loggerMessage.getMessage() + ". Filtered. Overall " + filteredItem.getFilterInfo().getCount() + " messages", loggerMessage.getParams());
				numOfLoggedMessages.incrementAndGet();
			}

			@Override
			public void onFilteringStarted(LoggerMessage loggerMessage) {
				loggerMessage.getLogger().log(Level.INFO, loggerMessage.getMessage() + ". Filtering duplicate logs...", loggerMessage.getParams());
				numOfLoggedMessages.incrementAndGet();
			}
		});

		IntStream.range(0, 100).forEach(i -> {
			boolean run = expSet.run(loggerMessage);
			Assert.assertEquals(i == 0, run);
		});

		Thread.sleep(filterMillis * 2); // Give a chance to purge the items.

		Assert.assertEquals(3, numOfLoggedMessages.get());
	}

	@Test
	public void testLogBombingUsageWithDefaults() throws InterruptedException {
		int filterMillis = 100;
		DuplicateLogFilter logFilter = new DuplicateLogFilter(filterMillis);
		Logger logger = Logger.getLogger(DuplicateActionFilterByInsertTimeTest.class.getName());

		IntStream.range(0, 100).forEach(i -> logFilter.log(new LoggerMessage(logger, Level.INFO, "Hello {0}", "Alik")));

		Thread.sleep(filterMillis * 2); // Give a chance to purge the items.
	}

	private class DuplicateLogFilter extends DuplicateActionFilterByInsertTime<LoggerMessage> {
		DuplicateLogFilter(int filterMillis) {
			super(filterMillis);
			addListener(new DuplicateActionFilterByInsertTime.Listener<LoggerMessage>() {
				@Override
				public void onFilteringFinished(FilteredItem<LoggerMessage> filteredItem) {
					filteredItem.getItem().getLogger().log(Level.INFO, filteredItem.getItem().getMessage() + ". Filtered. Overall " + filteredItem.getFilterInfo().getCount() + " messages", filteredItem.getItem().getParams());
				}

				@Override
				public void onFilteringStarted(LoggerMessage loggerMessage) {
					loggerMessage.getLogger().log(Level.INFO, loggerMessage.getMessage() + ". Filtering duplicate logs...", loggerMessage.getParams());
				}
			});

		}

		void log(LoggerMessage loggerMessage) {
			run(loggerMessage);
		}
	}

	public class LoggerMessageThatIncrements extends LoggerMessage {
		LoggerMessageThatIncrements(Logger logger, Level level, String message, Object... params) {
			super(logger, level, message, params);
		}

		@Override
		public void run() {
			super.run();
			numOfLoggedMessages.incrementAndGet();
		}
	}

	public static class LoggerMessage implements Runnable {
		private final Logger logger;
		private final Level level;
		private final String message;
		private final Object[] params;

		LoggerMessage(Logger logger, Level level, String message, Object... params) {
			this.logger = logger;
			this.level = level;
			this.message = message;
			this.params = params;
		}

		@Override
		public void run() {
			logger.log(level, message, params);
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;
			LoggerMessage that = (LoggerMessage) o;
			return Objects.equals(logger, that.logger) &&
					Objects.equals(level, that.level) &&
					Objects.equals(message, that.message) &&
					Arrays.equals(params, that.params);
		}

		@Override
		public int hashCode() {

			int result = Objects.hash(logger, level, message);
			result = 31 * result + Arrays.hashCode(params);
			return result;
		}

		public Logger getLogger() {
			return logger;
		}

		public String getMessage() {
			return message;
		}

		public Object[] getParams() {
			return params;
		}

		@SuppressWarnings("unused")
		public Level getLevel() {
			return level;
		}
	}
}
