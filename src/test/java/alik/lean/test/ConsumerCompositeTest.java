package alik.lean.test;

import alik.lean.ConsumerComposite;
import org.junit.Test;

public class ConsumerCompositeTest {

	@Test
	public void testExceptionThrown() {
		ConsumerComposite<Object> consumers = new ConsumerComposite<>();
		consumers.add(o -> {
			throw new RuntimeException();
		});
		consumers.accept(null); // Should not throw exception.
	}
}
