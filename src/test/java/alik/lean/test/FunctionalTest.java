package alik.lean.test;

import alik.lean.Functional;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.util.concurrent.atomic.AtomicReference;

public class FunctionalTest {

	private static final AtomicReference<Object> TRASH = new AtomicReference<>(0);

	@Test
	public void testUsingCheckedExceptionWithLambdas() {
		use(Functional.toRunnable(Functional.unchecked(this::functionThatDeclaresException)));
		use(Functional.toRunnable(Functional.toFunction(Functional.unchecked(this::predicateThatDeclaresException))));
		use(Functional.toRunnable(Functional.toFunction(Functional.unchecked(this::supplierThatDeclaresException))));
		use(Functional.toRunnable(Functional.toFunction(Functional.unchecked(this::consumerThatDeclaresException))));
		use(Functional.unchecked(this::runnableThatDeclaresException));
	}

	private void use(Runnable runnable) {
		Assertions.assertThatThrownBy(runnable::run).isInstanceOf(MyException.class);
	}


	private Boolean functionThatDeclaresException(Integer i) throws MyException {
		TRASH.set(i); // Do something with i in order to not see it in warnings.
		throw new MyException();
	}

	private boolean predicateThatDeclaresException(Integer i) throws MyException {
		functionThatDeclaresException(i);
		return true;
	}

	private Boolean supplierThatDeclaresException() throws MyException {
		return predicateThatDeclaresException(1);
	}

	private void consumerThatDeclaresException(Integer i) throws MyException {
		functionThatDeclaresException(i);
	}

	private void runnableThatDeclaresException() throws MyException {
		functionThatDeclaresException(1);
	}

	@Test
	public void testRunnableToFunction() {
		Functional.toFunction(() -> TRASH.set(0)).apply(null);
	}
}
