package alik.lean.test;

import alik.lean.Exceptions;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import static alik.lean.test.MyException.EXCEPTION_TEXT;

public class RethrowUncheckedTest {

	private static void throwAsUnchecked() {
		try {
			throw new MyException();
		} catch (Exception e) {
			throw Exceptions.rethrowUnchecked(e);
		}
	}

	@Test
	public void testExceptionNotDeclared() {
		try {
			throwAsUnchecked();
		} catch (Exception e) {
			Assertions.assertThat(e).isInstanceOf(MyException.class);
			Assertions.assertThat(e.getMessage()).isEqualTo(EXCEPTION_TEXT);

		}
	}
}
